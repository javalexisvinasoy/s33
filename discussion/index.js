// console.log("Hello")

// Javascript Synchronous and Asynchronous
// JS is by default synchronous, it means that only one state,ent is executed at a time.

/*console.log('Hello world')*/

/*console.log("Hello World")
for(i = 0; i <= 1000; i++) {
	console.log(i)
};
console.log("Hello Again")*/

// Asynchronous mean that we can proceed to execute other statements/code block, while time consuming code is running in the background.


// Getting All Posts

/*
	> The Fetch API allows you to asynchronously request for a resources(data).

	> A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value.

	Syntax:
		fetch("URL");
*/

/*
	A promise maybe in 1 of the 3 possible states:
		1. fulfilled
		2. rejected
		3. pending
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax:
		fetch("URL")
		.then((response)=>{//line code})
*/

fetch("https://jsonplaceholder.typicode.com/posts")

// By using the response.status we only check if the promise is fulfilled or rejected
// ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

// .then((response) => console.log(response.status))


// Use the "json" method form the "response" object to convert the date retrieved into JSON format to be used in our application.

.then((response) => response.json())
// Using multiple "then" methods that creates a "promsise chain"
// .then((json) => console.log(json));


// Displyas each title of the post
// .then((json) => {
// 	json.forEach(posts => console.log(posts.title))
// })

async function fetchData() {

	// Await - it waits for the "fetch" method to complete, then it stores the value in the result variable.
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	// Result returned by fetch is a promise return
	console.log(result)
	// The returned "response" is an object
	console.log(typeof result)
	// We cannot access the content of the "response" by directly accesing it's body property
	console.log(result.body)

	// Converts the data from the "response" object to JSON format.
	let json = await result.json()
	console.log(json)
}

fetchData();

/*------------------------------------------*/


// Getting a specific Post
// (retrieve, /posts/:id, GET)


// ":id" is a wildcard where you can put the unique identifier as value to show specific data /resources.

fetch("https://jsonplaceholder.typicode.com/posts/10")
.then((response) => response.json())
.then((json) => console.log(json))


/*------------------------------------------*/


// Creating a Post

/*
	Syntax:
		fetch("URL", options)
		.then((response) => {line code})
		.then((json) => {line code})
*/

fetch("https://jsonplaceholder.typicode.com/posts/", {
	// HTTP Method
	method:"POST",
	// Specifies the content that it will pass in JSON format
	headers: {
		"Content-Type": "application/json"
	},

	// Sets the content/body data of the "Request" objects to be sent to the backend/server
	body: JSON.stringify({
		title: "New Post",
		body: "Hello Batch 197",
		userId: 1
	})

})

// response of the server based on the request.
.then((response) => response.json())
.then((json) => console.log(json))


/*------------------------------------------*/


// Updating a post
// (update, /posts/:id, PUT)


// PUT methos is used to update the whole object/ document
fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated post",
		body: "Hello again Batch 197!",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json))


// Patch 

fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post"
	})
})

.then((response) => response.json())
.then((json) => console.log(json))


// Deleting
// (delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "DELETE"
})

