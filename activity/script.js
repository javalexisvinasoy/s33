// console.log("Activity")


fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())


async function fetchTodos() {

	//Part 3
	let response = await fetch("https://jsonplaceholder.typicode.com/todos")

	let resJson = await response.json()
	console.log(resJson)

	// Part 4
	let getTitle = resJson.map((json) => 'title: '+ json.title);
	console.log(getTitle)
}

fetchTodos();


// Part 5

fetch('https://jsonplaceholder.typicode.com/todos/25')
.then((res) => res.json())
.then((json) => console.log(json))

// Part 6

fetch('https://jsonplaceholder.typicode.com/todos/25')
.then((res) => res.json())
.then((json) => {
	console.log(`The title: ${json.title}` + " on the list, has a status of " + `completed: ${json.completed}`)
});

//Part 7

async function createTodo(){
		let response = await fetch('https://jsonplaceholder.typicode.com/todos', 
		{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body:JSON.stringify({
				userId: 6,
				completed: true,
				title: "New To do"
			})


		})

		let resJson = await response.json();
		console.log(resJson);
	}
createTodo();
		
// Part 8

async function updateTodo(){
	let response = await fetch('https://jsonplaceholder.typicode.com/todos/25', 
	{
		method: "PUT",
		headers : {
			"Content-type": "application/json"
		},
		body: JSON.stringify({
			title: "This is the updated to do"
		})

	})

	let resJson = await response.json()
	console.log(resJson)
}
updateTodo();

// Part 9

async function updateStructure(){

	let response =await fetch('https://jsonplaceholder.typicode.com/todos/25',
	{
		method: "PUT",
		headers: {
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			title: "Updated Structure",
			description: "updated to do list with a different data Structure",
			status: "Pending",
			date_completed: "Pending",
			userId: 74747
		})
	})

	let resJson = await response.json()
	console.log(resJson)
}

updateStructure();

async function patchTodo(){

	let response =await fetch('https://jsonplaceholder.typicode.com/todos/25',
	{
		method: "PATCH",
		headers: {
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			title: "This is a patched to do.",
		})
	})

	let resJson = await response.json()
	console.log(resJson)
}

patchTodo();

async function updateCompleteTodo(){

	let response =await fetch('https://jsonplaceholder.typicode.com/todos/25',
	{
		method: "PATCH",
		headers: {
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			completed: true,
			date_completed: '2022-08-27'
		})
	})

	let resJson = await response.json()
	console.log(resJson)
}

updateCompleteTodo();


async function deleteTodo(){

	let response =await fetch('https://jsonplaceholder.typicode.com/todos/25',
	{
		method: "DELETE",
	})

	let resJson = await response.json()
	console.log(resJson)
	console.log()
}

deleteTodo();